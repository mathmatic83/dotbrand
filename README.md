# dotBrand Frontend

### Table of Contents

- [Description](#description)
- [TODO](#todo)
- [References](#references)
- [License](#license)

## Description

這是從DotBrand專案中抽出的template部分，提供給前端團隊進行customer form的表單互動功能製作
專案本體使用Django進行製作，template部分見template以及static檔案夾

customer_form_workplace.html為從template中移出並移除python標籤的單獨檔案，將從這邊開始製作

## TODO
[文件](https://docs.google.com/document/d/1ZKRz2DILMrKnMux9-P8S0hVubdjicp1wvgewO2yJGrA/edit?usp=sharing)


#### API Reference

頁面預計使用兩個API:
1. character頁面取得選項
2. color頁面取得第一層以及第二層色碼

[測試頁面]()

##### 1. 取得character選項

<span style="color:red">實際連結待更新</span>

link: [https://www.dotbrand.com.tw/api/character_options/]()
###### response sample:
```javascript
character_option:
{ "option1": "['ELITE', 'REBEL']",
  "option2": "['CONVENTIONAL', 'EXPLORER']",
  "option3": "['WISDOM', 'INNOVATION']",
  "option4": "['CAREGIVER', 'HERO']",
  "option5": "['LOVER', 'PLAYFUL']",
}
```

##### 2. 取得color色碼
link: [https://www.dotbrand.com.tw/api/color_code/]()

response的key預計有9-12個組，為major color的選項；value為一list, 預計有9-12個值，為choose exact color的選項
###### response sample:
```javascript
color_option:
{ "#FF0000": "['#330000', '#550000', '#770000', '#990000', '#cc0000', '#ff0000', '#e06666', '#ea9999', '#f4cccc']",
  "#FF0000": "['#330000', '#550000', '#770000', '#990000', '#cc0000', '#ff0000', '#e06666', '#ea9999', '#f4cccc']"
}
```

[Back To The Top](#read-me-template)

---

## References
[Back To The Top](#read-me-template)

---

## License

Copyright (c) [2022] [DotBrand Project]

[Back To The Top](#read-me-template)

---

