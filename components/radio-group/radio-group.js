var preLabel = sufLabel = '';
var radioIndex = 0;
$('radio-group').each(function () {
    var radioGroup = $(this);
    var leftLabel = radioGroup.attr('pre-label');
    var rightLabel = radioGroup.attr('suf-label');
    var value = radioGroup.attr('value');
    var id = radioGroup.attr('id');
    var idString = id ? `id="${id}"` : '';
    var initDirection = '';
    disabled = radioGroup.attr('disabled');
    preLabel = leftLabel;
    sufLabel = rightLabel;
    if ([leftLabel, rightLabel].indexOf(value) !== -1) initDirection = leftLabel === value ? 'left' : 'right';
    $this = radioGroup.replaceWith(`
        <div class="radio-group ${disabled ? 'disabled' : ''}" value="${value ? value : ''}" ${idString}>
            <label class="radio-group__label left-label">${leftLabel}</label>
            <div class="radio-group__content ${initDirection}">
                <button></button>
                <button></button>
                <div class="circle"></div>
            </div>
            <label class="radio-group__label right-label">${rightLabel}</label>
        </div>`);
    radioIndex += 1;
})
$('.radio-group__content > button:first-child').click(function () {
    var $this = $(this);
    var radioGroup = $this.parent('.radio-group__content').parent('.radio-group');
    var disabled = radioGroup.attr('disabled') 
        || radioGroup.attr('class').indexOf('disabled') !== -1;
    if (!disabled) {
        $this.parent('.radio-group__content').removeClass('right');
        $this.parent('.radio-group__content').addClass('left');
        radioGroup.attr('value', preLabel);
    }
});
$('.radio-group__content > button + button').click(function () {
    var $this = $(this);
    var radioGroup = $this.parent('.radio-group__content').parent('.radio-group');
    var disabled = radioGroup.attr('disabled') 
        || radioGroup.attr('class').indexOf('disabled') !== -1;
    if (!disabled) {
        $this.parent('.radio-group__content').removeClass('left');
        $this.parent('.radio-group__content').addClass('right');
        radioGroup.attr('value', sufLabel);
    }
});